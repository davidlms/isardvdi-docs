# IsardVDI Community

Welcome to the IsardVDI community! This page is designed to provide resources and support for users and contributors of the IsardVDI platform.

## Getting Started

If you're new to IsardVDI, here are some resources to help you get started:

- [IsardVDI Documentation](https://docs.isardvdi.com/en/latest/): The official documentation for IsardVDI, which includes installation instructions, user guides, and developer documentation.
- [IsardVDI GitLab Repository](https://gitlab.com/isardvdi/isard): The source code repository for IsardVDI, where you can find the latest releases, contribute code, and report issues.
- IsardVDI Forum (in construction): A community forum for users and contributors of IsardVDI, where you can ask questions, share ideas, and get support from the community.

## Contributing to IsardVDI

IsardVDI is an open-source project, and we welcome contributions from anyone interested in helping to improve the platform. Here are some ways you can get involved:

- [GitLab Issues](https://gitlab.com/isardvdi/isard/-/issues): If you encounter a bug or have a feature request, you can report it on GitLab Issues. This is also a good place to find issues that need help from the community.
- [GitLab Merge Requests](https://gitlab.com/isardvdi/isard/-/merge_requests): If you want to contribute code to IsardVDI, you can submit a merge request on GitLab. Before doing so, please read our [Contributing Guidelines](https://gitlab.com/isardvdi/isard/-/blob/master/CONTRIBUTING.md).
- Community Forum (in construction): If you have questions about contributing to IsardVDI, or want to discuss a potential contribution, you can use the development category of our forum to connect with other contributors.

## Community Guidelines

We want to foster a welcoming and supportive community around IsardVDI. To that end, we ask that all members of the community abide by the following guidelines:

- Be respectful: We welcome diversity and inclusivity in our community, and ask that everyone treat each other with respect and kindness.
- Be constructive: When offering feedback or criticism, please do so in a constructive and helpful manner.

## Conclusion

We hope you find this page helpful in getting started with and contributing to IsardVDI. Thank you for being a part of our community!
