# Introducción

Hemos organizado la documentación con:

- [**Manual de Usuario**](../user/index.es/) como utilizar IsardVDI para crear escritorios, plantillas, despliegues, ...
- [**Manual Administrador**](../admin/index.es) para los roles **gestor** y **administrador** con la interfaz de administración avanzada de gestión para gestionarlo todo. Los gestores solo podrán gestionar con su categoría mientras los administradores lo verán todo y también elementos del sistema como descargas o hipervisors.
- [**Guía de instalación**](../install/requirements) tiene detalles técnicos para instalar y configurar una plataforma de virtualización IsardVDI completa.
- Se está rehaciendo la documentación de la [**API**](../api/index.es/), pero ahora tiene algunos consejos que muestran como hacerlo todo a través del API.
- En [**Comunidad**](../community/info.es/) encontraréis enlaces para mantener el contacto y contribuir al proyecto.
- [**Casos de uso**](../use_cases/index.es/) es útil para obtener ideas que podáis adaptar a vuestro caso de uso.

# IsardVDI

La infraestructura de escritorio virtual (VDI) es una tecnología que permite a los usuarios acceder a escritorios virtuales alojados en un servidor remoto o en una infraestructura en la nube. VDI se está volviendo cada vez más popular en las organizaciones, ya que proporciona varios beneficios sobre los entornos de escritorio tradicionales, incluida la seguridad mejorada, una administración más fácil y una mayor flexibilidad para el trabajo remoto.

IsardVDI es una solución VDI de **código abierto** que proporciona una plataforma completa de gestión de infraestructura de escritorio virtual. Permite a los administradores crear, configurar y administrar aplicaciones y escritorios virtuales para usuarios en múltiples dispositivos, independientemente de su ubicación. IsardVDI utiliza contenedores Docker para proporcionar una plataforma de virtualización ligera y escalable, lo que la hace altamente eficiente y fácil de implementar.

<iframe width="800" height="400" src="https://www.youtube.com/embed/modrGJ_Szyk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Siéntase libre de probar el programario en [IsardVDI Gitlab](https://gitlab.com/isard/isardvdi) o abrirnos un ticket con su solicitud en [Gitlab tickets de IsardVDI](https://gitlab.com/isard/isardvdi/-/issues/new).
