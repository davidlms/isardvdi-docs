# RemoteFX - Cómo escanear dispositivos mediante visor RDP

## Descripción

RemoteFX es una tecnología desarrollada por Microsoft que permite la virtualización de gráficos y recursos de hardware en entornos de escritorio remoto. Está diseñado para mejorar la experiencia de usuario al proporcionar una calidad gráfica excepcional y un rendimiento sólido en entornos de escritorio virtualizados.

RemoteFX está estrechamente relacionado con el uso de Remote Desktop Protocol (RDP) en el escritorio remoto de Windows. RDP es un protocolo de Microsoft que permite a los usuarios acceder y controlar de forma remota un ordenador o un servidor desde otro dispositivo. RemoteFX extiende las capacidades de RDP al agregar funcionalidades de virtualización de gráficos y USB.

En cuanto al escaneo de dispositivos mediante RDP en el escritorio remoto de Windows, RemoteFX permite que los dispositivos USB conectados a la máquina local se escaneen y estén disponibles en la sesión del escritorio remoto. Esto significa que los usuarios pueden utilizar dispositivos USB como impresoras, escáneres, cámaras web, dispositivos de almacenamiento y otros periféricos directamente desde el escritorio remoto, como si estuvieran conectados localmente.

En relación con el visor RDP, RemoteFX es una tecnología clave para habilitar el escaneo de dispositivos USB en los escritorios virtuales. Permite que los usuarios que acceden a través del visor RDP en los escritorios virtuales utilicen y accedan a los dispositivos USB conectados a su máquina local desde el entorno virtualizado.


## Cómo implementar RemoteFX

En **el equipo anfitrión** y el **escritorio virtual** (si es S.O. Windows) se deben implementar las siguientes directivas de grupo.

!!! Tip
    Si el escritorio virtual tiene un S.O Windows, las **plantillas prediseñadas que ofrece IsardVDI** ya vienen **preparadas con las configuraciones necesarias que necesita RemoteFX** que se explican a continuación, por lo que sólo se debe aplicar esta configuración en el **equipo anfitrión** si el usuario utiliza estas plantillas para crear escritorios.

    De lo contrario, si el usuario ha creado un **escritorio Windows personalizado por su cuenta**, aquí se explica cómo configurar su sistema para poder escanear dispositivos USB extraños mediante protocolo RDP en Windows. 

1. **Abrir herramienta "Editar directiva de grupo"**

    ![](remote_fx.es.images/directiva_de_grupo.png){ width="30%" }

2. **Configuración del equipo >> Plantillas administrativas >> Componentes de Windows >> Servicios de Escritorio Remoto**

    ![](remote_fx.es.images/servicios_escritorio_remoto.png){ width="60%" }

3. **Cliente de conexión a Escritorio Remoto**

    1. **Redirección de dispositivos USB RemoteFX >> Permitir la redirección RDP de otros dispositivos USB RemoteFX compatibles desde este dispositivo**

        Hay que habilitar la directiva y aplicarla para todos los usuarios:

        ![](remote_fx.es.images/directiva1.png)

        ![](remote_fx.es.images/habilitar_directiva.png){ width="60%" }

4. **Host de sesión de escritorio remoto**

    1. **Entorno de sesión remota >> RemoteFX para Windows Server 2008 R2 >> Configurar RemoteFX**

        Hay que habilitar la directiva:

        ![](remote_fx.es.images/directiva_configurar_remote_fx.png)

    2. **Redirección de dispositivo o recurso**

        Hay que habilitar y deshabilitar las siguientes directivas:

        **Habilitar**

        * Permitir la redirección de la reproducción de audio y vídeo
        * Permitir la redirección de la grabación de audio
        * Permitir redirección de zona horaria
        * Permitir redirección de automatización de la interfaz de usuario

        **Deshabilitar**

        * No permitir el redireccionamiento de la captura de vídeo
        * No permitir redirección del Portapapeles
        * No permitir redirección de puertos COM
        * No permitir redirección de unidad
        * No permitir redirección de puertos LPT
        * No permitir redirección de dispositivo Plug and Play compatible
        * No permitir redirección de ubicación
        * No permitir la redirección de dispositivos de tarjeta inteligente
        * No permitir el redireccionamiento de WebAuth

        ![](remote_fx.es.images/host_lista_directivas.png){ width="80%" }


Una vez hecho todo esto, se recomienda **reiniciar equipo y escritorio** para que los cambios se apliquen correctamente.

!!! Tip
    El siguiente paso solamente es necesario si el **sistema operativo del escritorio virtual es Windows**, y el escritorio no ha derivado de una de las **plantillas prediseñadas que ofrece IsardVDI**, donde **ya se encuentran estas configuraciones**.

1. En el escritorio virtual se abre una línea de comandos CMD y se ejecuta ``gpupdate/force`` y se **reinicia el escritorio**.

    ![](remote_fx.es.images/gpupdate_force.png){width="60%"}


2. En el anfitrión, se edita el fichero descargado ***.rdp*** del **visor** del escritorio.

    ![](viewers.es.images/visor_rdp_14.png){width="50%"}

    1. **Recursos Locales >> Más**

        Se puede comprobar el **dispositivo detectado por el equipo** y listo para **escanearlo dentro del escritorio**.

        ![](viewers.es.images/visor_rdp_15.png){width="40%"}
        ![](viewers.es.images/visor_rdp_16.png){width="40%"}

        ![](viewers.es.images/visor_rdp_17.png){width="40%"}
        ![](viewers.es.images/visor_rdp_18.png){width="40%"}