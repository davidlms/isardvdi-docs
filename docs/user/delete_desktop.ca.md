# Esborrar escriptori

Per a esborrar un escriptori es prem el botó ![](delete_desktop.es.images/delete_desktop1.png).

![](delete_desktop.ca.images/delete_desktop2.png){width="80%"}

![](delete_desktop.ca.images/delete_desktop3.png){width="80%"}
