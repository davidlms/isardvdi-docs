# Edit Desktop

The following sections can be edited in addition to the desktop name, its description, and a descriptive image.

To edit a desktop, press the icon ![](edit_desktop.es.images/edit_desktop3.png)

![](./edit_desktop.images/edit_desktop1.png){width="90%"}


## Viewers

In this section you can select which [viewers](../user/viewers/viewers.md) you want to use to **access** the desktop. You can also select whether you want to start the desktop in full screen or not.

![](./create_desktop.images/viewers.png){width="90%"}


## RDP Login

In this section, you can add the login **user and password** for the **RDP viewer** (user created in the O.S of the desktop). This configuration is only necessary if you don't want to be authenticating yourself to the desktop by RDP all the time and if the RDP viewer is selected.

![](./create_desktop.images/rdp_login.png){width="60%"}


## Hardware

In this section you can edit the hardware you want to have on the desktop.

- **Videos**: by default **always selects the option Default**
- **Boot**: 
    - **Hard disk** if there is an operating system installed in the desktop
    - **CD/DVD** if a **[media](../advanced/media.md)** is assigned and you want to start it from this
- **Disk bus**: by default **always selects the option Default**
- **Networks**: list of network interfaces desired for the desktop.

![](./create_desktop.images/hardware.png){width="90%"}


## Reservables

In this section you can select the **profile of the GPU card** that you want to associate with the desktop (it is possible that they are not available).

![](./create_desktop.ca.images/hardware_2.png){width="90%"}


## Media

In this section you can add a [media](../advanced/media.md) of the user or one that has been shared with.

![](./create_desktop.es.images/media.png){width="90%"}


## Image

In this section you can select the **cover image** that the desktop will have in the main page.

![](./edit_desktop.images/image.png){width="90%"}