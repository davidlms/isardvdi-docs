
# Explicación de la tecnología vGPU

IsardVDI permite trabajar con la tecnología de **[Nvidia vWS](https://www.nvidia.com/es-es/design-visualization/virtual-workstation/)** que permite disponer de recursos de una tarjeta gráfica asociados a un escritorio virtual. Esta tecnología nos permite poder ejecutar software que requiere **recursos de GPU dedicados**, como programas de diseño 3D, animación, edición de vídeo, CAD o diseño industrial.

Cada tarjeta gráfica puede dividirse en diferentes perfiles con una cantidad de memoria reservada. En función del perfil seleccionado se dispone de más o menos tarjetas virtuales. En el siguiente gráfico se observa un servidor con una serie de tarjetas GPU instaladas. Cada tarjeta puede dividirse en vGPUs (virtual GPUs) que pueden mapearse a cada máquina virtual.

![](./gpus.images/o6SWmXS.png)

Veamos con un ejemplo el funcionamiento. Se dispone de una tarjeta **[Nvidia A40](https://www.nvidia.com/es-es/data-center/a40/)** y se quiere usar un programa como el SolidWorks para diseñar piezas 3D. Se utiliza un perfil con 2GB de memoria gráfica dedicada. 
Esta tarjeta gráfica dispone de 48GB de memoria, que puede dividirse en perfiles "2Q": el primer carácter "2" hace referencia a la cantidad de memoria reservada (2GB) y el segundo carácter "Q" hace referencia al licenciamiento vWS, el modo en que la tarjeta virtual se configura para trabajos creativos y técnicos que usan la tecnología **[Quadro de Nvidia](https://docs.nvidia.com/grid/10.0/grid-vgpu-user-guide/index.html#supported-gpus-grid-vgpu)**. 
Los diferentes perfiles y modos de funcionamiento disponibles para esta tarjeta están accesibles en el [manual de nvidia con los perfiles para la A40](https://docs.nvidia.com/grid/13.0/grid-vgpu-user-guide/index.html#vgpu-types-nvidia-a40). En el caso de 2Q tenemos:

- Memoria dedicada y reservada: 2GB
- Número máximo de vGPUs por GPU: 24

Diagramas de funcionamiento de la tecnología Nvidia vGPU:

![](./gpus.images/9qceuMB.png)

![](./gpus.images/qtAbMD3.png)

Para más información sobre el uso de la tecnología GPU sobre escritorios en IsardVDI, consultar el [apartado de reservas y perfiles](../../user/bookings.es.md).
