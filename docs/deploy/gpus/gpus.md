
# Explanation of vGPU technology

IsardVDI allows working with **[Nvidia vWS](https://www.nvidia.com/es-es/design-visualization/virtual-workstation/)** technology, which allows you to have resources from a graphics card associated with a virtual desktop. This technology enables us to run software that requires **dedicated GPU resources**, such as 3D design programs, animation, video editing, CAD, or industrial design.

Each graphics card can be divided into different profiles with a certain amount of reserved memory. Depending on the selected profile, you will have more or fewer virtual cards. The following image shows a server with a series of GPU cards installed. Each card can be divided into vGPUs (virtual GPUs) that can be mapped to each virtual machine.

![](./gpus.images/o6SWmXS.png)

Let's see how it works with an example. You have an **[Nvidia A40](https://www.nvidia.com/es-es/data-center/a40/)** card and want to use a program like SolidWorks to design 3D pieces. You will use a profile with 2GB of dedicated graphics memory. This graphics card has 48GB of memory, which can be divided into "2Q" profiles: the first character "2" refers to the amount of reserved memory (2GB), and the second character "Q" refers to the vWS licensing mode, the way in which the virtual card is configured for creative and technical work that uses **[Nvidia Quadro](https://docs.nvidia.com/grid/10.0/grid-vgpu-user-guide/index.html#supported-gpus-grid-vgpu)** technology. The different profiles and operating modes available for this card are accessible in the [Nvidia manual with profiles for the A40](https://docs.nvidia.com/grid/13.0/grid-vgpu-user-guide/index.html#vgpu-types-nvidia-a40). In the case of 2Q, you have:

- Dedicated and reserved memory: 2GB
- Maximum number of vGPUs per GPU: 24

Functioning diagrams of Nvidia vGPU technology:

![](./gpus.images/9qceuMB.png)

![](./gpus.images/qtAbMD3.png)

For more information on using GPU technology on desktops in IsardVDI, please consult the [section on bookings and profiles](../../user/bookings.md).