# Actualització

## Des d'Inici ràpid

### Manualment

**Recomanem revisar la nova versió de isardvdi.cfg.example per si hi ha nous paràmetres que vulguis configurar.**

Cada vegada que es realitza una actualització cal córrer el script build.sh que llegirà les variables de configuració de isardvdi.cfg. Després de fer un build.sh descarreguem la versió actualitzada dels contenidors i rearrenquem els que hagin estat modificats, IsardVDI hauria de tornar a estar operatiu.

```
cd path_to_isard_source
git pull
./build.sh
docker-compose pull
docker-compose up -d
```

### Automàtic

Recomanem configurar actualitzacions automàtiques mitjançant un cron que executi el següent [script](https://gitlab.com/isard/isardvdi/-/blob/main/sysadm/isard-upgrade-cron.sh).

**En cas de modificacions en la versió de isardvdi.cfg.example no es realitzarà l'actualització automàtica.**