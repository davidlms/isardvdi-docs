# Descàrregues

!!! info "Rols amb accés"

    Només els **administradors** tenen accés a aquesta característica.

En aquesta secció es troba els recursos ja descarregats del servei cloud d'IsardVDI.

Per anar a aquesta secció, es prem el botó de ![](downloads.images/downloads1.png)

![](downloads.images/downloads2.png)

## Dominis

Aquí es poden descarregar els escriptoris que són al núvol d'IsardVDI.

![](downloads.images/downloads3.png)

## Mitjans

Es troben els isos i floppys que proporciona IsardVDI.

![](downloads.images/downloads4.png)

## Vídeos

Els recursos de vídeo que proporciona IsardVDI.

![](downloads.images/downloads5.png)

## Plantilles de maquinari OS

Plantilles que vénen descarregades per defecte a IsardVDI per a diversos sistemes opertatius (virt-install)

![](downloads.images/downloads6.png)

## Visors

Aquí es poden descarregar els visors de Spice per a Windows.

![](downloads.images/downloads7.png)