# Direct viewer

This is used to share a desktop through a direct link to it, **without the need** for the user to be **registered** on the platform.

Press the button ![](direct_viewer.es.images/visor_directo3.png) of the desktop you want to share by link

![](./direct_viewer.images/direct_viewer1.png){width="80%"}

A dialog box will emerge where you have to select the checkbox so that the link appears and you can copy it

![](./direct_viewer.images/direct_viewer2.png){width="80%"}
