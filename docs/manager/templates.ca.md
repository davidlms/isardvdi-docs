# Plantilles

En aquesta secció es poden veure totes les plantilles que s'hagin creat de totes les categories.

![](./domains.images/domains7.png)

Si es prem la icona ![[+]](./domains.images/domains5.png), es pot veure més informació de la plantilla.

* Hardware: com el seu nom indica, és el maquinari que se li ha assignat a aquesta plantilla

* Allows: indica a qui se li ha donat permisos per utilitzar la plantilla

* Storage: indica l'UUID del disc associat a la plantilla i el seu ús

* Media: indica els mitjans associats a la plantilla, si n'hi ha cap

![Detalls de la plantilla](./domains.images/domains8.png)

## Activació

En habilitar una plantilla (enable), es permet a tots els usuaris amb els quals es comparteixi veure-la i utilitzar-la per crear escriptoris.

Per habilitar o deshabilitar una plantilla, simplement s'ha de fer clic a la casella de selecció de la columna "Enabled" de la taula i confirmar la selecció a la notificació emergent

![Checkboxes de les plantilles  habilitades](./domains.images/domains53.png)

Al fer clic al botó ![Oculta desactivat](./domains.images/domains49.png) situat a dalt a la dreta, les plantilles que no estan habilitades (visibles pels usuaris amb els quals estan compartides) no es mostraran a la taula. Al fer clic al botó ![Visualitza Desactivat](./domains.images/domains50.png) es tornaran a mostrar.

## Duplicar

Aquesta opció permet duplicar la plantilla a la base de dades, mentre que es manté el disc original.

La duplicació de plantilles es pot realitzar diverses vegades i és útil a alguns casos, com ara:

- Es vol compartir la plantilla en altres categories. Es podria duplicar la plantilla i assignar-la a un altre usuari en una categoria diferent amb permisos de compartició separats per la còpia nova i mantenir la versió original

L'eliminació d'una plantilla duplicada no eliminarà la plantilla original.

En duplicar una plantilla, es pot personalitzar la còpia nova:

* Nom de la nova plantilla
* Descripció de la nova plantilla
* Usuari assignat com a nou propietari de la plantilla
* Habilitar o deshabilitar la visibilitat de la plantilla als usuaris compartits
* Seleccionar els usuaris amb qui es vol compartir la nova plantilla

![](./domains.images/domains46.png)

## Esborrar

!!! danger "Acció no reversible"

    Actualment l'esborrat d'una plantilla i els seus escriptoris i plantilles derivats no es pot recuperar.
    Amb l'entrada de la MR de [paperera de reciclatge](https://gitlab.com/isard/isardvdi/-/merge_requests/1692)
    es podran recuperar durant un temps predefinit.

En eliminar una plantilla, apareixerà un modal que mostrarà tots els escriptoris creats a partir d'ella i qualsevol plantilla duplicada. Eliminar la plantilla comportarà l'eliminació de tots els dominis associats i els seus discs.

![](./domains.images/domains47.png)

!!! Warning "Dependències"
    L'eliminació de la plantilla comportarà l'eliminació de tots els dominis associats i els seus discs.

Tot i això, si la plantilla a eliminar és una duplicada d'una altra plantilla, no cal eliminar-les totes. Només s'haurien d'eliminar si també s'elimina la plantilla d'origen.

![](./domains.images/domains48.png)


# Plantilla com Servidor

Una plantilla que es marqui com a *servidor* farà que tots els escriptoris que s'en creïn siguin també servidors. Això vol dir que el sistema iniciarà sempre aquests escriptoris si els troba aturats.

Per a poder generar un servidor a partir d'una plantilla s'ha d'anar al panell de "Administració".

Es prem el botó ![](./template_server.ca.images/template_server1.png)

![](./template_server.ca.images/template_server2.png)

Una vegada allí es va a "Templates" sota l'apartat de "Manager".

Es prem el botó ![](./template_server.images/template_server3.png)

![](./template_server.images/template_server4.png)

Es prem el botó ![](./template_server.images/template_server5.png) de la plantilla que es vulgui convertir en server.

![](./template_server.images/template_server6.png)

Es prem el botó ![](./template_server.images/template_server7.png)

![](./template_server.images/template_server8.png)

I apareixerà una finestra de diàleg. S'ha de marcar la casella "Set it as server"

![](./template_server.images/template_server9.png)