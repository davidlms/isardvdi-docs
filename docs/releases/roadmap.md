# Roadmap

The IsardVDI team is a group of developers and sysadmins who are dedicated to providing a powerful and flexible virtual desktop infrastructure solution. We are constantly working to improve the software and add new features based on user feedback and emerging technologies.

These are some of the features coming soon:

1. **Custom Networks with OpenVSwitch**: IsardVDI is developing a new *isardvdi-ovs* container that will allow to bring OpenVSwitch in IsardVDI to another level. Focused on bringing autonomy to the users, this feature will allow creating an configuring own networks between desktops, with optional services like DHCP or network connection with NAT.

You can follow it's development mainly at [https://gitlab.com/isard/isardvdi/-/merge_requests/1045](https://gitlab.com/isard/isardvdi/-/merge_requests/1045)

2. **Personal units**: This feature will auto connect network storage units from a Nextcloud instance to the user virtual desktops. Also allowing to access this storage outside the virtual desktops, from IsardVDI web interface or even with nextcloud client app in any device. With this feature the *temporal desktops* feature will have more sense, as the user will have it's own personal network unit attached to his virtual desktops as a persistent storage, allowing to remove the virtual desktop on stop, so reducing the storage used.

You can follow it's development at [https://gitlab.com/isard/isardvdi/-/merge_requests/1259](https://gitlab.com/isard/isardvdi/-/merge_requests/1259)

3. **Event Queue System**: There are lots of features coming with the event queue system. This will allow to have *isard-storage* containers that will consume the actions added by user or scheduler. This actions can take long times and will be handled based on queues priority and consumer load. For example doing a disk sparsify, snapshot, convert, upload, download, ...

You can follow it's development at [https://gitlab.com/isard/isardvdi/-/merge_requests/1815](https://gitlab.com/isard/isardvdi/-/merge_requests/1815)

4. **Live Migration**: When you scale up IsardVDI to multiple hypervisors in infrastructure, you want to reduce the online servers based on demand. To allow smaller hypervisor shutdown times, the virtual desktops being used that are started in one hypervisor will be live migrated to another, without almost user notice of this.

5. **Recycle bin**: Desktops and templates won't be immediately removed and will be kept for a choosen interval in the system that will allow recovering before being definetly deleted.

A lot other bugfixes, improvements and minor features are being developed and tested, like:

- Allowing advanced user roles to remove templates
- Access virtual desktop http/https from main IsardVDI domain
- Importing your VirtualBox desktops as IsardVDI desktops
- Exporting your IsardVDI desktop to KVM
- Virtual desktop snapshots
- Edit deployment desktops
- New manager and admin system status pannel
- Storage pools and features to move disks between them
- Logs about desktops and users actions
- Email events
- ...